export type DataObject = {
    label: string,
    name: string
};