import {Component, Inject} from '@angular/core';
import { FormGroup } from '@angular/forms';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {DataObject} from '../common/data.object'


export interface DialogData {
  hours: string;
  day: string;
  fields: Array<DataObject>;
}

/**
 * @title Dialog Overview
 */
@Component({
  selector: 'app-dialog',
  templateUrl: 'dialog.component.html',
})

export class DialogComponent {

  constructor(
    public dialogRef: MatDialogRef<DialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {}

  ngOnInit() {
    console.log(this.data.fields);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}