import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { CalendarOptions, DateSelectArg, EventApi, EventClickArg } from '@fullcalendar/angular'; // the main connector. must go first
import { DataObject } from '../common/data.object';
import { DialogComponent } from '../dialog/dialog.component';
import { createEventId } from './event-utils';


@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.sass']
})
export class CalendarComponent implements OnInit {

  hours: string;

  calendarOptions: CalendarOptions = {
    initialView: 'dayGridMonth',
    height: "auto",
    weekends: false,
    weekNumbers: true,
    weekNumberCalculation: "ISO",
    weekNumberFormat: {week: "numeric"},
    editable: true,
    selectable: true,
    locale: "fi",
    //dateClick: this.handleDateClick.bind(this), // bind is important!
    select: this.handleDateSelect.bind(this),
    eventClick: this.handleEventClick.bind(this),
    eventsSet: this.handleEvents.bind(this),
    events: [
      { title: 'event 1', date: '2021-04-01' },
      { title: 'event 2', date: '2021-04-02' }
    ]
  };
  currentEvents: EventApi[] = []
  dialogData: string;
  

  constructor(public dialog: MatDialog) {
    this.hours = "";
    this.dialogData = "";
   }

  ngOnInit(): void {
  }

  handleDateClick(arg: { dateStr: string; }) {
    this.openDialog(arg.dateStr, []);
    //TODO:
    //add the hours from the dialog to calendar on the date clicked
    //this.addEvent(this.hours);

  }

  handleDateSelect(selectInfo: DateSelectArg) {
    //const title = prompt('Select title');
    var start = "start";
    var end = "end";
    this.openDialog(selectInfo.startStr, [{label: start, name: ""}, {label: end, name: ""}]);
    console.log(selectInfo);
    const calendarApi = selectInfo.view.calendar;
    const title = this.hours;

    if (title) {
      calendarApi.addEvent({
        id: createEventId(),
        title,
        start: selectInfo.startStr,
        end: selectInfo.endStr,
        allDay: selectInfo.allDay
      })
    }
  }

  handleEventClick(clickInfo: EventClickArg) {
    if (confirm(`Are you sure you want to delete the event '${clickInfo.event.title}'`)) {
      clickInfo.event.remove();
    }
  }

  handleEvents(events: EventApi[]) {
    this.currentEvents = events;
  }
  //TODO:
  //create create calendar event
/*     addEvent(event: string): void {
    this.calendarOptions.
  }  */

  toggleWeekends() {
    this.calendarOptions.weekends = !this.calendarOptions.weekends // toggle the boolean!
  }

  openDialog(day: string, fields: Array<DataObject>): void {
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '250px',
      data: {hours: this.hours, day: day, fields: fields}
    });

    dialogRef.afterClosed().subscribe((result: string) => {
      console.log('The dialog was closed');
      this.dialogData = result;
      console.log(this.dialogData)
    });
  }

}
